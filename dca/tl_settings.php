<?php

$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] =
  str_replace(
    '{smtp_legend',
    '{mailgun_legend},Mailgun_key,Mailgun_domain;{smtp_legend',
    $GLOBALS['TL_DCA']['tl_settings']['palettes']['default']
  );

$GLOBALS['TL_DCA']['tl_settings']['fields']['Mailgun_key'] =
  [
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['Mailgun_key'],
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50')
  ];

$GLOBALS['TL_DCA']['tl_settings']['fields']['Mailgun_domain'] =
  [
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['Mailgun_domain'],
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50')
  ];
