<?php

namespace Mailgun;

class Email
{
    protected $mg;

    protected $mb;

    protected $arrContaoData;

    public function __construct()
    {
        $this->mg = new \Mailgun\Mailgun($GLOBALS['TL_CONFIG']['Mailgun_key'], new \Http\Adapter\Guzzle6\Client());

        $this->mb = $this->mg->MessageBuilder();
    }

    public function __set($var, $value)
    {
        switch ($var)
        {
            case 'subject':
            case 'from':
            case 'fromName':
            case 'html':
            case 'text':
                $this->arrContaoData[$var] = $value;
                break;
        }
    }

    public function __get($var)
    {
        return $this->arrContaoData[$var];
    }

    public function attachFile($strFile)
    {
        $this->mb->addAttachment($strFile);
    }

    /**
     * As far as I can see, mailgun only accepts single reply to addresses. We therefore only use the
     * first, if multiple are specified
     *
     * @return bool|mixed
     */
    public function replyTo()
    {
        $arrReplyTo = self::compileRecipients(func_get_args());

        self::convertToMbSyntaxAndAdd(array_slice($arrReplyTo, 0, 1, true), 'setReplyToAddress');
    }

    public function sendCc()
    {
        $this->convertToMbSyntaxAndAdd(self::compileRecipients(func_get_args()), 'addCcRecipient');
    }

    public function sendBcc()
    {
        $this->convertToMbSyntaxAndAdd(self::compileRecipients(func_get_args()), 'addBccRecipient');
    }

    public function addHeader($strKey, $strValue)
    {
        $this->mb->addCustomHeader($strKey, $strValue);
    }

    public function sendTo()
    {
        $this->mb->setFromAddress($this->arrContaoData['from'], ['full_name' => $this->arrContaoData['fromName']]);
        $this->mb->setSubject($this->arrContaoData['subject']);
        if ($this->arrContaoData['html'])
        {
            $this->mb->setHtmlBody($this->arrContaoData['html']);
        }
        $this->mb->setTextBody($this->arrContaoData['text'] ?: strip_tags($this->arrContaoData['html']));

        $this->convertToMbSyntaxAndAdd(self::compileRecipients(func_get_args()), 'addToRecipient');

        $objResult = $this->mg->post(self::getMgDomain() . '/messages', $this->mb->getMessage(), $this->mb->getFiles());

        return $objResult->http_response_code == 200 ? true : false;
    }

    public static function getMgDomain()
    {
        if ($GLOBALS['TL_CONFIG']['Mailgun_domain']) return $GLOBALS['TL_CONFIG']['Mailgun_domain'];

        // Try to take our FQDN without subdomains and prepend 'mg.'
        // this FAILS with domains like 'xy.co.uk'
        $arrHost = array_reverse(explode('.', $_SERVER['SERVER_NAME']));
        return 'mg.' . $arrHost[1] . '.' . $arrHost[0];
    }

    /**
     * Extract the e-mail addresses from the func_get_args() arguments
     *
     * @param array $arrRecipients The recipients array
     *
     * @return array An array of e-mail addresses
     */
	protected static function compileRecipients($arrRecipients)
    {
        $arrReturn = array();

        foreach ($arrRecipients as $varRecipients)
        {
            if (!is_array($varRecipients))
            {
                $varRecipients = \StringUtil::splitCsv($varRecipients);
            }

            // Support friendly name addresses and internationalized domain names
            foreach ($varRecipients as $v)
            {
                list($strName, $strEmail) = \StringUtil::splitFriendlyEmail($v);

                $strName = trim($strName, ' "');
                $strEmail = \Idna::encodeEmail($strEmail);

                if ($strName != '')
                {
                    $arrReturn[$strEmail] = $strName;
                }
                else
                {
                    $arrReturn[] = $strEmail;
                }
            }
        }

        return $arrReturn;
    }

    protected function convertToMbSyntaxAndAdd($arrRecipients, $strMbFunction)
    {
        if (is_array($arrRecipients))
        {
            foreach ($arrRecipients as $key => $value)
            {
                // I assume it is done this weird way in Contao (addresses sometimes as keys, sometimes as values)
                // since e-mail-addresses must be unique, names not.
                if (\Validator::isEmail($key))
                {
                    $this->mb->$strMbFunction($key, ['full_name' => $value]);
                }
                else
                {
                    $this->mb->$strMbFunction($value);
                }
            }
        }
    }

}