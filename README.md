Contao-Mailgun
==============

This is a [Contao](http://contao.org) module integrating the Mailgun API.

It replaces the native email function in Contao, so you need to specify your Mailgun API key, and optionally your Mailgun domain name, in the Contao settings for the plugin to work.

Installation
------------

1. Place the mailgun folder into your system/modules folder.
2. Place your Mailgun API key into the field provided in the Contao general settings. Optionally, add your mailgun domain if it's not "mg.yourdomain.com".

Bugs
----

If you find a bug, feel free to submit a report on [Bitbucket](https://bitbucket.org/philvdb/mailgun/issues/new).

The software is provided as is by Dr. Philip von der Borch ([101010-Webdesign](http://101010-webdesign.de)), use at your own risk, no warranty offered.
