<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2016 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'Mailgun',
	'GuzzleHttp',
	'Http',
	'Psr',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'Mailgun\Email'                                             => 'system/modules/mailgun/classes/Email.php',
));

require_once(TL_ROOT . '/system/modules/mailgun/vendor/autoload.php');