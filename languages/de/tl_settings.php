<?php

$GLOBALS['TL_LANG']['tl_settings']['mailgun_legend'] = 'Mailgun';

$GLOBALS['TL_LANG']['tl_settings']['Mailgun_key'][0] = 'Private key';
$GLOBALS['TL_LANG']['tl_settings']['Mailgun_key'][1] = 'Ihr Privater Schlüssel für die Mailgun-API';

$GLOBALS['TL_LANG']['tl_settings']['Mailgun_domain'][0] = 'Mailgun domain';
$GLOBALS['TL_LANG']['tl_settings']['Mailgun_domain'][1] = 'Die Domain, die in Mailgun registriert ist. Wenn Sie dieses Feld leer lassen, wird \'mg.ihre-domain.de\' dynamisch aus der aktuellen Domain generiert (Cave: funktioniert nicht mit Domains wie xy.co.uk).';
