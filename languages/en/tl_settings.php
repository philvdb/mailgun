<?php

$GLOBALS['TL_LANG']['tl_settings']['mailgun_legend'] = 'Mailgun';

$GLOBALS['TL_LANG']['tl_settings']['Mailgun_key'][0] = 'Private key';
$GLOBALS['TL_LANG']['tl_settings']['Mailgun_key'][1] = 'Your private key for the Mailgun API';

$GLOBALS['TL_LANG']['tl_settings']['Mailgun_domain'][0] = 'Mailgun domain';
$GLOBALS['TL_LANG']['tl_settings']['Mailgun_domain'][1] = 'Your registered domain with Mailgun. If left blank, will attempt \'mg.yourdomain.com\' generated from the current domain the user is seeing, with subdomains cut off. Will fail with domains like xy.co.uk, as mg.co.uk would be generated.';
